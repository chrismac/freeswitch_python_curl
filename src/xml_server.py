from gevent import monkey; monkey.patch_all()
from gevent.pywsgi import WSGIServer
import gevent
import web
import hashlib
import rethinkdb as r
import json
import phonenumbers

urls = (
    '/freeswitch', 'index',
     '/api/v1/', 'api'
)

render = web.template.render('templates/', cache=False)

def convert_e164(num, region):
     (country, local) = region
     if num[:2] == '81':
          #lets get out of here as 814 account, will ring directly
          return (num,'local')

     if len(num) == 7:
          if local:
               num = local+num

     try:
          x = phonenumbers.parse(num, country)
          number = phonenumbers.format_number(x, phonenumbers.PhoneNumberFormat.E164)
          #number = phonenumbers.format_number(x, phonenumbers.PhoneNumberFormat.NATIONAL)
          return (number[1:], 'e164')
     except:
          return (number, 'failed')

def hash_password(domain, username, password):
     hash = hashlib.md5()
     hash.update(username + ":" + domain + ":" + password)
     password_hash = hash.hexdigest()
     password_param = "a1-hash"
     return password_param, password_hash

def get_user(id,domain):
     conn = r.connect('localhost', 28015).repl()
     user = r.db('freeswitch').table('users').get(id).run()
     return user

def get_sip_auth(sip_user, domain):
     user = get_user(int(sip_user), domain)
     print user
     try:
          (name, a1hash) = hash_password(domain, str(user['user']), user['password'])
          return a1hash
     except:
          return False

def get_dialplan(sip_user, number, domain):
     user = get_user(int(sip_user), domain)
     print user
     try:
          (num, dest) = convert_e164(number, (user['country'], user['local']))
          reg_num = len(num) - 1 
          return (domain, num, '^(\d+)$', dest)
     except: False

def get_rate(user, domain, dest):
     user = get_user(int(user), domain)
     conn = r.connect('localhost', 28015).repl()
     rates = r.db('freeswitch').table('rates')

     num = phonenumbers.parse('+'+dest, None)
     type = phonenumbers.number_type(num)
     res = rates.get_all(user['rate_table'],index='table').filter({'country_code': str(num.country_code), 'number_type':type}).run()
     for i in res:
          print i
          return i['rate']
          
class index:
    def POST(self):
        web.header('Content-Type', 'freeswitch-xml')
        data = web.input()
        #for bit in data:
        #     print bit + ' : '
        
        if 'section' in data and data['section'] == 'directory':
             if 'action' in data and data['action'] == 'sip_auth':
                  a1hash = get_sip_auth(data['sip_auth_username'], data['sip_auth_realm'])
                  if a1hash:
                       resp = render.directory(data['domain'],data['sip_auth_username'],a1hash)
                  else:
                       resp = render.response()
             elif 'purpose' in data and data['purpose'] == 'gateways':
                  resp = render.gateway()
             else:
                  a1hash = get_sip_auth(data['user'], data['domain'])
                  if a1hash:
                       resp = render.directory(data['domain'],data['user'],a1hash)
                  else:
                       resp = render.response()

        if data['section'] == 'dialplan':
             if 'Caller-Destination-Number' in data:
                  if len(data['Caller-Destination-Number']) < 7:
                       resp = render.features()
                       print resp
                       return resp
                       
                  if 'variable_domain_name' in data:
                       dialplan = get_dialplan(data['Caller-Username'],data['Caller-Destination-Number'], data['variable_domain_name'])
                  elif 'variable_sip_from_host' in data:
                       dialplan = get_dialplan(data['Caller-Username'],data['Caller-Destination-Number'], data['variable_sip_from_host'])
                  print len(data['Caller-Destination-Number'])
                  if dialplan:
                       (dom, num, reg, dest) = dialplan 
                       if dest == 'local':
                            resp = render.local_dialplan(dom, num, reg)
                       else:
                            rate = get_rate(data['Caller-Username'], dom, num)
                            resp = render.dialplan(dom, num, reg, rate, data['Caller-Username'])
                  else:
                       resp = render.response()
             else:
                  resp = render.response()
             
        print resp
        return resp

class api:
     def POST(self):
          return False
     def GET(self):
          data = web.input()
          if 'user' in data:
               user = get_user(int(data['user']), 'something here')
               if user:
                    resp = user
               else:
                    resp = {'user':'null'}
          else:
               resp = {'user':'null'}
          web.header('Content-Type', 'application/json')
          return json.dumps(resp)

if __name__ == "__main__":
    application = web.application(urls, globals()).wsgifunc()
    print 'Serving on 8088...'
    WSGIServer(('', 8088), application).serve_forever()

