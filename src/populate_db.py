import rethinkdb as r

r.connect('localhost', 28015).repl()
#r.db('freeswitch').table_drop('users').run()
#r.db('freeswitch').table_drop('rates').run()
r.db('freeswitch').table_create('users',primary_key='user').run()
r.db('freeswitch').table_create('rates').run()

users = r.db('freeswitch').table('users')
rates = r.db('freeswitch').table('rates')


users.insert(
    [{u'domains': [u'sip99.sipaccount.com',u'114.23.48.7'], u'country': u'NZ', u'user': 8195555, u'password': u'cti123', u'local': u'03', u'rate_table': u'test'},
         {u'domains': [u'sip99.sipaccount.com',u'114.23.48.7'], u'country': u'NZ', u'user': 8195556, u'password': u'cti123', u'local': u'03', u'rate_table': u'test'},
         {u'domains': [u'sip99.sipaccount.com',u'114.23.48.7'], u'country': u'NZ', u'user': 8195557, u'password': u'cti123', u'local': u'03', u'rate_table': u'test'}
	 ]).run()

users.index_create('domains').run()
users.index_create('country').run()
users.index_create('local').run()

#need to create loop from phonenumber lib
rates.insert(
    [{u'table':u'test',u'country_code':u'64',u'number_type':0, u'rate': 0.035}, #landline == 0, from phonenumbers.number_type
     {u'table':u'test',u'country_code':u'64',u'number_type':1, u'rate': 0.25}, #mobile
     {u'table':u'test',u'country_code':u'64',u'number_type':3, u'rate': 0.00}, #toll free
     {u'table':u'test',u'country_code':u'64',u'number_type':4, u'rate': 2.50}  #premium rate
         ]).run()

rates.index_create('table').run()
rates.index_create('country').run()

for r in users.run():
	print r
